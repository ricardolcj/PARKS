from scrapli.driver import GenericDriver
import re
from time import sleep

from login import auth_username, auth_password

device = {
   "host": "172.16.160.2",
   "auth_username": auth_username,
   "auth_password": auth_password,
   "auth_strict_key": False,
}

conn = GenericDriver(**device)
conn.open()
conn.send_command('terminal length 0')

hostname = conn.get_prompt()[:-1]
comando = input(hostname+"# ")

comando = 'show int gpon1/1 onu status not-'

resposta = conn.send_command(comando).result

serial = r"[a-z0-9]{12}"

matches = re.findall(serial, resposta)

print(matches)

conn.close()