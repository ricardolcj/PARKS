##ROTINA DE EXCLUSÃO DE BACKUPS MK COM MAIS DE d DIAS DE IDADE
import os
from time import time, ctime

diretorio = "/home/fsi/files/MK"
d = 15 #dias

def dias_passados(segundos):
    #converte o argumento passado para dias
    return segundos/86400

def main():

    data_atual = time()

    #Lista de nomes dos arquivos do diretorio
    lista_arquivos = os.listdir(diretorio)

    for arquivo in sorted(lista_arquivos):
        nome_arquivo = diretorio + '/' + arquivo
        data_criacao = os.path.getctime(nome_arquivo)

        tempo_segundos = data_atual-data_criacao

        #Se o tempo d criação do arquivo tiver passado de D dias, apagar
        if(dias_passados(tempo_segundos) > d):
            print(arquivo)
            ##os.remove(nome_arquivo)
        else:
            print("NÃO APAGAR")
if __name__ == "__main__":
    main()

