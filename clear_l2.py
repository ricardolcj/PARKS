import telnetlib
from time import sleep
from mylib.OLTS import lista_OLTS
from mylib.logins import user_parks, pass_parks

def clear_l2(olt, ip_gerencia):

    #Conecta TELNET
    try:
        t = telnetlib.Telnet(ip_gerencia, timeout=5)
        t.write(b"\n")
        t.read_until(b"Username:")
        t.write(bytes(user_parks, 'UTF-8'))
        t.write(b"\n")
        t.read_until(b"Password:")
        t.write(bytes(pass_parks, 'UTF-8'))
        t.write(b"\n")
    except:
        print(f"Erro ao conectar na {olt}")

    try:
        #Envio da instrução para a OLT, convertendo de string pra Bytes
        t.write(bytes("clear l2 mac-address-table\n", 'UTF-8'))
        
        #Espera o envio do comando para a OLT
        sleep(5)
        
        print(f"Tabela MAC da {olt} limpa")
        
        #Fecha a sessão telnet
        t.close()

    except Exception:
        print(f"Erro ao executar o comando na {olt}")

def main():

    for olt, ip_gerencia in zip(lista_OLTS.keys(), lista_OLTS.values()):
        clear_l2(olt, ip_gerencia)

if __name__ == "__main__":
    main()