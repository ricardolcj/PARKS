import telnetlib
from os import makedirs as criar_diretorio
from os.path import join as caminho
from datetime import datetime
import logging
import time
from queue import Queue
import os
from mylib.OLTS import lista_OLTS
from mylib.logins import user_parks, pass_parks, user_ftp_backup, pass_ftp_backup

logging.basicConfig(filename='backup_OLTs.log', encoding='UTF-8',
format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %H:%M:%S', level=logging.DEBUG)

servidor = ''##IP do servidor de backup

#Pasta a partir do diretório do script onde os novos diretórios serão criados
raiz = 'files/OLT_NEW'

MAX_TENTATIVAS = 30

def fazer_backup(olt, ip_gerencia, pasta):

    agora = datetime.now().strftime('%Y_%m_%d_%H_%M_%S') 
    nome_arquivo = f"{olt}-{agora}.bin"

    #Conecta TELNET
    try:
        t = telnetlib.Telnet(ip_gerencia, timeout=5)
        t.write(b"\n")
        t.read_until(b"Username:")
        t.write(bytes(user_parks, 'UTF-8'))
        t.write(b"\n")
        t.read_until(b"Password:")
        t.write(bytes(pass_parks, 'UTF-8'))
        t.write(b"\n")
    except:
        logging.error(f'Erro ao acessar a {olt}')

    try:
        logging.info(f"{olt} iniciando...")

        #Hora de início, nome do arquivo e criação do comando para mandar para a OLT
       
        comando = f"copy startup-config ftp://{servidor}/{pasta}/{nome_arquivo} {user_ftp_backup} {pass_ftp_backup}\n"
        
        #Envio da instrução para a OLT, convertendo de string pra Bytes
        t.write(bytes(comando, 'UTF-8'))
        
        #Criação de log de possíveis erros
        t.write(b"show uptime\n")
        leitura = t.read_until(b'idle', timeout=3).decode("UTF-8")
        #Fecha a sessão telnet
        t.close()
        
        logging.info(leitura.split('\n')[-4])
        logging.info(f"{olt} terminou backup!")

    except IndexError:
        ##logging.info(t.read_until(b'idle', timeout=3).decode("UTF-8"))
        logging.info(f"{olt} terminou backup!")

    except Exception as err:
        logging.error(f'Erro no backup: {olt}{err=}, {type(err)=}')
    
    return nome_arquivo

def validar_arquivos(pasta):

    #Iniciar fila
    fila = Queue()

    #Lista de nomes de arquivo
    arquivos = os.listdir(pasta)

    for i in range(len(arquivos)):
        arquivos[i] = arquivos[i].split("-")[0]

    #Comparar início do nome com as OLTs, ver se todas tem um arquivo.
    #Se faltar em alguma, colocar OLT na fila
    for olt in lista_OLTS.keys():
        if(olt not in arquivos):
            fila.put(olt)
  
    #Para cada arquivo, verificar o tamanho. Se  for = 0, adicionar à fila
    for nome_arquivo in os.listdir(pasta):
        backup = caminho(pasta,nome_arquivo)
        tamanho_backup = os.path.getsize(backup)
        if(tamanho_backup==0):
            fila.put(nome_arquivo.split("-")[0])
            os.remove(backup)
    
    #Fila inicial pronta
    logging.error(f'Falha na validação: {list(fila.queue)}')
    #Iniciar dicionario D {OLT: qtd_tentativas]}
    lista_olts = list(fila.queue)
    D = dict(zip(lista_olts, [0]*len(lista_olts)))
   
    execs = 0

    #Enquanto a fila não estiver vazia
    while not fila.empty():
        
        execs = execs + 1
        #HARD STOP
        if(execs > MAX_TENTATIVAS):
            logging.error(f'Não foi possível finalizar o backup da(s) {list(fila.queue)}.\nTentativas adicionais por OLT {D}')
            return False

        #Remover OLT da fila, fazer um novo backup
        olt = fila.get()
        logging.info(f"Tentativa de backup da {olt}")
        nome_arquivo = fazer_backup(olt, lista_OLTS[olt], pasta)

        #Incrementa quantidade de tentativas
        D[olt] = D[olt] + 1
        time.sleep(30)
        
        #RETORNAR NOME DO ARQUIVO NOVO
        backup = caminho(pasta,nome_arquivo)
        logging.info(f"Novo backup criado {backup}")
        #CHECAR TAMANHO
        try:
            tamanho_backup = os.path.getsize(backup)
            
            if(tamanho_backup==0):
                #SE FOR IGUAL A 0, COLOCAR A OLT NA FILA NOVAMENTE
                fila.put(olt)
                #EXCLUI O ARQUIVO ZERADO
                os.remove(backup)

        #TRATA QUANDO ARQUIVO NÃO EXISTE POR ALGUMA RAZÃO
        except OSError:
            logging.error(f'Arquivo {nome_arquivo} não encontrado para checagem')
            fila.put(olt)
            
    logging.info(D)

    logging.info(f'Validação finalizada...\nTentativas adicionais por OLT {D}')

    return True

def main():

    hoje = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    ano, mes, dia, horario_inicio = hoje.split("_", 3)

    pasta = f"{raiz}/{ano}/{mes}/{dia}"

    logging.info(f'BACKUP INICIANDO...')
    
    try:
        criar_diretorio(caminho(pasta))
        logging.info("Diretório criado!")
    except FileExistsError:
        logging.error('Erro ao criar o diretorio. Pasta ja existe')

    for olt, ip_gerencia in zip(lista_OLTS.keys(), lista_OLTS.values()):
        time.sleep(5)
        fazer_backup(olt, ip_gerencia, pasta)
        logging.info(f"Primeiro backup da {olt} finalizado")
       
    validar_arquivos(pasta) 
    
    logging.info(f'BACKUP FINALIZADO.')

if __name__ == "__main__":
    main()