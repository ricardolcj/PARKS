from math import ceil
from telnetlib import Telnet
from re import fullmatch, findall
import threading
from time import perf_counter
from mylib.OLTS import lista_OLTS
from mylib.logins import user_parks, pass_parks

N_threads = len(lista_OLTS)

def encontrar_prks(prks, inicio, fim):

   for i in range (inicio, fim):
        olt = list(lista_OLTS.keys())[i]
        ip_gerencia = lista_OLTS[olt]
        #Conecta TELNET
        try:
            t = Telnet(ip_gerencia, timeout=5)
            t.write(b"\n")
            t.read_until(b"Username:")
            t.write(bytes(user_parks, 'UTF-8'))
            t.write(b"\n")
            t.read_until(b"Password:")
            t.write(bytes(pass_parks, 'UTF-8'))
            t.write(b"\n")
        except:
            print(f'Erro ao acessar a {olt}')
            continue

        try:      
            comando = f"show gpon onu {prks} su\n"
            
            #Envio da instrução para a OLT, convertendo de string pra Bytes
            t.write(bytes(comando, 'UTF-8'))
        
            leitura = t.read_until(b'Unknown command', timeout=0.5).decode("UTF-8")

            if(len(olt)==5):
                olt = olt+'\t'
            
            if "Unknown command" not in leitura:
                #leitura = t.read_until(b'Serial', timeout=1).decode("UTF-8")
                #print(leitura)
                interface = findall("gpon1/.*", leitura)[0].split(" ")[0]

                print(f"{olt}: PRKS ENCONTRADO NA INTERFACE {interface}")
            
            #Fecha a sessão telnet
            t.close()
            
        except Exception as err:
            print(f"Erro ao procurar o {prks} na {olt}")

def lancar_threads(prks):
    fila = []
    qtd_olts = len(lista_OLTS)

    inicio = 0
    step = ceil(qtd_olts/N_threads)
    fim = step

    for i in range (N_threads):
        if (fim > qtd_olts): fim = qtd_olts
        
        fila.append(threading.Thread(target=encontrar_prks,args=(prks, inicio, fim),))
        inicio = fim
        fim = fim + step

    for i in fila:
        i.start()
    
    for i in fila:
        i.join()

def main():

    try:

        prks = input('''O PROGRAMA VAI PROCURAR O PRKS INFORMADO EM TODAS AS OLTS. Em caso de bug, favor reportar [Cerqueira]\nCopie o prks a seguir: ''')
        regex = "prks00[a-z0-9]{6}$"

        while(True): 
            while(not fullmatch(regex, prks)):
                prks = input("Formato de prks inválido. Confira o serial e tente novamente: ")
            print(f"Pesquisando {prks}...")
            inicio = perf_counter()
            lancar_threads(prks)
            fim = perf_counter()
            print("Busca finalizada em %.2f segundos. Para terminar, basta fechar a janela..." % (fim-inicio))
            print("==========================================================================")
            prks = input("Copie o prks a seguir: ")
    except KeyboardInterrupt:
       print("Saindo...")
if __name__ == "__main__":
    main()